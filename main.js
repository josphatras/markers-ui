dragging = false;
lastX = 0;
lastY = 0;
marginLeft = 0;
marginTop = 0;
canvasoffsetLeft = 0;
canvasoffsetTop = 0;
points = [];
amenityPoints = [];
planAmenities = [];
drawing = false;
popUpSelect = false;
selectedShapeId = '';
localStorage.clear();
section = '';
base64Img = '';
canvasScale = 1;
newAmenities = false;

selAmenity = {};

routing();
getColors();

function routing(){
  var url = location.hash.slice(1) || '/';
  routes = url.split('/');  
  switch(routes['0']) {
    case "new":
      section = 'new';
      break;
    case "view":
      section = 'view';
      break;
    case "edit":
      section = 'edit';
      break;
    case "addamenities":
      section = 'addamenities';
      newAmenities = true;
      break;      
  }
}

markersColors = {};
apiData = {};

window.onload = function() {
  var optionForm = document.getElementById("typologyForm");
  typology = optionForm.elements["typology"].value;
  if (window.Event) {
    document.captureEvents(Event.MOUSEMOVE);
  }
  window.addEventListener("keydown",keydown,false);
  window.addEventListener('hashchange',()=>{
    routing();
    if (section !== 'new') {
      getPlan(routes['1']);
    }else{
      createCanvas(false, false);
    }
  });
  switch(section) {
    case "new":
      document.getElementById("admin-control").style.display = 'block';
      document.getElementById("edit-link").style.display = 'none';
      document.getElementById("save_new_plan").style.display = 'block';
      document.getElementById("add_shapes").style.display = 'none';
      document.getElementById("plan_file_upload").style.display = 'block';
      break;
    case "view":
      document.getElementById("admin-control").style.display = 'none';
      document.getElementById("edit-link").style.display = 'none';
      document.getElementById("plan_file_upload").style.display = 'none';
      break;
    case "edit":
      document.getElementById("admin-control").style.display = 'block';
      document.getElementById("edit-link").style.display = 'block';
      document.getElementById("save_new_plan").style.display = 'none';
      document.getElementById("add_shapes").style.display = 'block';    
      document.getElementById("plan_file_upload").style.display = 'none';
      break;
    case "addamenities":
      document.getElementById("admin-control").style.display = 'block';
      document.getElementById("edit-link").style.display = 'block';
      document.getElementById("save_new_plan").style.display = 'none';
      document.getElementById("add_shapes").style.display = 'block';    
      document.getElementById("plan_file_upload").style.display = 'none';
      break;      
  }
};

function typologySelection(){
  var optionForm = document.getElementById("typologyForm");
  typology = optionForm.elements["typology"].value;  
  createCanvas(false, false);
}

function getCursorXY(e) {
  var canvasSection = document.getElementById('drawMakersCanvas');
  // console.log(drawing, newAmenities);
  if (drawing) {
    points.push([
      (e.pageX - canvasSection.offsetLeft)/canvasScale, 
      (e.pageY - canvasSection.offsetTop)/canvasScale
      ]);
    createCanvas(true, false);
  }
  if (newAmenities) {
    amenityPoints.push([{"points" : [
      (e.pageX - canvasSection.offsetLeft)/canvasScale, 
      (e.pageY - canvasSection.offsetTop)/canvasScale
      ]}, {"details": "bla bla"}]);
    createCanvas(false, false);
    $('#amenityFormModal').modal('show');
  }  
  // console.log(points, amenityPoints);
}

window.addEventListener("load", () => {
  if (section !== 'new') {
    getPlan(routes['1']);
  }else{
    createCanvas(false, false);
  }
})

function keydown(e)
{
  if (drawing) {
   if(e.key == 'Enter'){
     createCanvas(true, true);
   }
  }

  if (newAmenities) {
    createCanvas(false, true);
  }
}


function amenities(ctx){
  if (amenityPoints['0'] !== undefined) {
    amenityPoints.forEach(function(data){
      var points = data['0'].points;
      drawCircles(ctx, points);
    })  
  }

  if (section == 'addamenities') {
    if (planAmenities.length >0) {
      planAmenities.forEach(function(data){
        var points = JSON.parse(data.points);
        drawCircles(ctx, points);
      })      
    }
  }

  if (selAmenity.points !== undefined) {
    drawCircles(ctx, JSON.parse(selAmenity.points));
  }
}


function createCanvas(draw, closePath){
  const canvas = document.querySelector("#drawMakersCanvas");
  const ctx = canvas.getContext("2d");

  //Resizing
  canvas.height = 1600;
  canvas.width = 2000;
  if (draw) {
    canvas.onclick = getCursorXY;
  }

  if (newAmenities) {
     canvas.onclick = getCursorXY;
  }

  drawSection(ctx, closePath);
  
  var handleMousedown = function(e){
    var evt = e || event;
    dragging = true;
    lastX = evt.clientX;
    lastY = evt.clientY;
    var canvasSection = document.getElementById('drawMakersCanvas');
    if (!drawing) {
      pointInDrawing('click', ctx, e.pageX - canvasSection.offsetLeft, e.pageY - canvasSection.offsetTop);
      pointInCircle('click', ctx, e.pageX - canvasSection.offsetLeft, e.pageY - canvasSection.offsetTop);
    }
    e.preventDefault();
  };

  canvas.addEventListener('mousedown', handleMousedown, false);
  
  var handleMousemove = function(e){
    console.log('mouse move');
      var evt = e || event;
      var deltaX = evt.clientX - lastX;
      var deltaY = evt.clientY - lastY;
      lastX = evt.clientX;
      lastY = evt.clientY;
      if (dragging) {
          marginLeft += deltaX;
          marginTop += deltaY;
          canvas.style.marginLeft = marginLeft + "px";
          canvas.style.marginTop = marginTop + "px";
      }
      var canvasSection = document.getElementById('drawMakersCanvas');
      if(dragging){
          canvasoffsetLeft = marginLeft;
          canvasoffsetTop = marginTop;      
      }
      if (!drawing) {
        if (selAmenity.points == undefined) {
          pointInDrawing('move', ctx, e.pageX - canvasSection.offsetLeft, e.pageY - canvasSection.offsetTop);      
        }
      }
      e.preventDefault();
  }; 
  canvas.addEventListener('mousemove', handleMousemove, false);
  
  var handleMouseup = function(){
    dragging = false;
  }; 
  canvas.addEventListener('mouseup', handleMouseup, false);

  //:::::::::::::::::::::::::::::::::::::::::::;\
  var handleScroll = function(event){
    canvas.removeEventListener('mousedown', handleMousedown, false);
    canvas.removeEventListener('mousemove', handleMousemove, false);
    canvas.removeEventListener('mouseup', handleMouseup, false);

     if (event.deltaY < 0){
       if (canvasScale < 3) {
        canvasScale += 0.001;
       }
     }else if (event.deltaY > 0){
       if (canvasScale > 0.5) {
        canvasScale -= 0.001;
       }
     }
     if (section == 'new') {
      document.querySelector("#drawMakersCanvas").style.background = "url('" + base64Img +"') left top/" + 1000*canvasScale + "px auto no-repeat";
     }
      ctx.clearRect(0,0,1600,1000);
      createCanvas(true, true);
    canvas.removeEventListener('wheel',handleScroll,false);
  };
  canvas.addEventListener('wheel',handleScroll,false);
}

function drawSection(ctx, closePath){
  ctx.scale(canvasScale, canvasScale);
  ctx.beginPath();
  drawLines(ctx);
  ctx.stroke();

  var prev = JSON.parse(localStorage.getItem(typology));
  if (prev) {
    if (closePath) {
      prev.push(points);
    }
  }

  if (closePath) {
    if (prev) {
      localStorage.setItem(typology, JSON.stringify(prev));
    }else{
      localStorage.setItem(typology, JSON.stringify([points]));
    }
    points.length = 0;
  }
  if (section == 'view' || section == 'edit' || section == 'addamenities') {
    if (section == 'edit' || section == 'addamenities') {
      displayShapes(ctx);
    }
    planView(ctx);    
  }else{
    displayShapes(ctx);
  }
}

function planView(ctx){
  // *******************************************************************************
 document.querySelector("#drawMakersCanvas").style.background = "url(http://127.0.0.1:3000/" + apiData.plan['0'].img_path +") left top/" + 1000*canvasScale + "px auto no-repeat";

  var shapes = [];  
  switch(typology) {
    case "1bhk":
      shapes = apiData.drawing.bhk1;
      break;
    case "2bhk":
      shapes = apiData.drawing.bhk2;
      break;
    case "2bhks":
      shapes = apiData.drawing.bhks2;
      break;
    case "3bhk":
      shapes = apiData.drawing.bhk3;
      break;
     case "studio":
      shapes = apiData.drawing.studio;
      break;
  } 
  var fontsize=10;
  var fontface='verdana';
  if (shapes) {
    var data=[];
    shapes.forEach(function(items){
    ctx.beginPath();
    JSON.parse(items.points).forEach(function(point){
    ctx.lineTo(point['0'], point['1']);
    })
    ctx.closePath();
    ctx.strokeStyle = "#FFF";
      switch(typology) {
        case "1bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk1 + ', 0.5)';
          break;
        case "2bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk2 + ', 0.5)';
          break;
        case "2bhks":
          ctx.fillStyle = 'rgba(' + markersColors.bhks2 + ', 0.5)';
          break;
        case "3bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk3 + ', 0.5)';
          break;
         case "studio":
          ctx.fillStyle = 'rgba(' + markersColors.studio + ', 0.5)';
          break;
      }       
      ctx.fill();
      ctx.font=fontsize+'px '+fontface;
      ctx.fillStyle = "#000000";

      var imgDataData=ctx.getImageData(0,0,2000,1600).data;
      for(var i=0;i<imgDataData.length;i+=4){
          data.push(imgDataData[i+3]);
      } 
      // console.log(data);     
      var center = getRectCenter(JSON.parse(items.points));
      ctx.fillText((items.wing == null) ? '': items.wing ,center.centerx,center.centery);
      ctx.stroke();
    })
  }  
  amenities(ctx);
}


function getRectCenter(shape){
      var minX;
      var minY;
      var maxX;
      var maxY;
      var items = [];

      shape.forEach(function(point){
        if (maxY !== undefined) {
          if (maxY < point['1']*canvasScale) {
            maxY = point['1']*canvasScale;
          }
        }else{
          maxY = point['1']*canvasScale;
        }

        if (maxX !== undefined) {
          if (maxX < point['0']*canvasScale) {
            maxX = point['0']*canvasScale;
          }
        }else{
          maxX = point['0']*canvasScale;
        }

        if (minY !== undefined) {
          if (minY > point['1']*canvasScale) {
            minY = point['1']*canvasScale;
          }
        }else{
          minY = point['1']*canvasScale;
        }

        if (minX !== undefined) {
          if (minX > point['0']*canvasScale) {
            minX = point['0']*canvasScale;
          }
        }else{
          minX = point['0']*canvasScale;
        }
      })
      var centerx = minX + ((maxX-minX)/2);
      var centery = minY + ((maxY-minY)/2);
      return {centerx, centery};
}

function drawLines(ctx){
  points.forEach(function(point){
    ctx.lineTo(point['0'], point['1']);
  })
}

function drawCircles(ctx, points){
  ctx.beginPath();
  ctx.arc(points['0'], points['1'], 10, 0, 2 * Math.PI);
  ctx.strokeStyle = "#FFF";
  ctx.fillStyle = 'rgba(255,0,0, 0.5)';
  ctx.fill(); 
}

function displayShapes(ctx){
  var shapes = JSON.parse(localStorage.getItem(typology));  
  if (shapes) {
    shapes.forEach(function(items){
      ctx.beginPath();
      items.forEach(function(point){
        ctx.lineTo(point['0'], point['1']);
      })
      ctx.closePath();
       ctx.strokeStyle = "#FFF";
      if (section !== 'edit') {
        switch(typology) {
          case "1bhk":
            ctx.fillStyle = 'rgba(' + markersColors.bhk1 + ', 0.5)';
            break;
          case "2bhk":
            ctx.fillStyle = 'rgba(' + markersColors.bhk2 + ', 0.5)';
            break;
          case "2bhks":
            ctx.fillStyle = 'rgba(' + markersColors.bhks2 + ', 0.5)';
            break;
          case "3bhk":
            ctx.fillStyle = 'rgba(' + markersColors.bhk3 + ', 0.5)';
            break;
           case "studio":
            ctx.fillStyle = 'rgba(' + markersColors.studio + ', 0.5)';
            break;
        }       
      }else{
        ctx.fillStyle = 'rgba(255,0,0, 0.5)';
      }
      ctx.fill();
      ctx.stroke();
    })
  }
}

function btnSelection(name){
  var canvasSection = document.querySelector("#drawMakersCanvas");
  var drag = document.getElementById('drag');
  var draw = document.getElementById('draw');
  if (name == 'drag') {
    drag.style.background = '#000';
    draw.style.background = '#009999';
    canvasSection.style.cursor = 'grab';
    drawing = false;
    createCanvas(true, false);
    // dragging = true;
  }else if (name == 'draw') {
    drag.style.background = '#009999';
    draw.style.background = '#000';
    canvasSection.style.cursor = 'default';
    drawing = true;
    createCanvas(true, false);
    // dragging = false;
  }
}

function undoDrawing(){
  points.pop();
  createCanvas(true, false);
}

function pointInDrawing(eventType, ctx, x, y){
  if (section == 'view' || section == 'edit' ) {
    // var shapes = [];
    switch(typology) {
      case "1bhk":
        shapes = apiData.drawing.bhk1;
        break;
      case "2bhk":
        shapes = apiData.drawing.bhk2;
        break;
      case "2bhks":
        shapes = apiData.drawing.bhks2;
        break;
      case "3bhk":
        shapes = apiData.drawing.bhk3;
        break;
       case "studio":
        shapes = apiData.drawing.studio;
        break;
    }     
  }else{
  var shapes = JSON.parse(localStorage.getItem(typology));  
  }
  if (shapes) {
    var isInDrawing = false;
    var displayX;
    var displayY;
    var item = [];
    var details = [];
    shapes.forEach(function(shape){
      var minX;
      var minY;
      var maxX;
      var maxY;

      var items = [];
      if (section == 'view' || section == 'edit') {
        items = JSON.parse(shape.points);
      }else{
        items = shape;
      }

      items.forEach(function(point){
        if (maxY !== undefined) {
          if (maxY < point['1']*canvasScale) {
            maxY = point['1']*canvasScale;
          }
        }else{
          maxY = point['1']*canvasScale;
        }

        if (maxX !== undefined) {
          if (maxX < point['0']*canvasScale) {
            maxX = point['0']*canvasScale;
          }
        }else{
          maxX = point['0']*canvasScale;
        }

        if (minY !== undefined) {
          if (minY > point['1']*canvasScale) {
            minY = point['1']*canvasScale;
          }
        }else{
          minY = point['1']*canvasScale;
        }

        if (minX !== undefined) {
          if (minX > point['0']*canvasScale) {
            minX = point['0']*canvasScale;
          }
        }else{
          minX = point['0']*canvasScale;
        }
      })
      var checkPoint = inDrawing(x,y,[minX, minY, maxX, maxY]);
      if (checkPoint) {
        details = shape;
        isInDrawing = true;
        item = items;
        displayX = maxX+canvasoffsetLeft;
        displayY = minY+canvasoffsetTop;
      }
    })
          
    var detailPop = document.getElementById('detail-pop');
    if (isInDrawing) {
      if (eventType == 'click') {
        if (section !== 'view') {
          popUpSelect = true;
        }
      }
      ctx.clearRect(0,0,1600,1000);
      if (section == 'view' || section == 'edit') {
        if (section == 'edit') {
          displayShapes(ctx);
        }        
        planView(ctx);    
      }else{
        displayShapes(ctx);
      }      
      ctx.beginPath();
      item.forEach(function(point){
        ctx.lineTo(point['0'], point['1']);
      })
      ctx.closePath();
      switch(typology) {
        case "1bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk1 + ', 1)';
          break;
        case "2bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk2 + ', 1)';
          break;
        case "2bhks":
          ctx.fillStyle = 'rgba(' + markersColors.bhks2 + ', 1)';
          break;
        case "3bhk":
          ctx.fillStyle = 'rgba(' + markersColors.bhk3 + ', 1)';
          break;
         case "studio":
          ctx.fillStyle = 'rgba(' + markersColors.studio + ', 1)';
          break;
      }
      ctx.strokeStyle = "#fff";
      ctx.fill();
      ctx.stroke();
      
      detailPop.style.display = 'block';
      if (section == 'view' || section == 'edit') {
        detailPop.style.margin = displayY + 'px ' + '0 0 ' + displayX + 'px';
      }else{
        detailPop.style.margin = displayY + 'px ' + '0 0 ' + displayX + 'px';
      }
      var popTypollogy = document.getElementById('pop-typollogy');
      popTypollogy.innerHTML = typology; 
      document.getElementById('pop-unit').innerHTML = details.unit; 
      document.getElementById('pop-sqmt').innerHTML = details.sqmt; 
      document.getElementById('pop-sqft').innerHTML = details.sqft; 
      document.getElementById('pop-ticket-size').innerHTML = details.ticket_size;
      selectedShapeId = details.id;
    }else{
      if (section !== 'view') {
        if (popUpSelect) {
/*          ctx.clearRect(0,0,1600,1000);
          displayShapes(ctx);
          detailPop.style.display = 'none';*/
        }else{
          if (true) {}
          ctx.clearRect(0,0,1600,1000);
          if (section == 'view' || section == 'edit') {
            if (section == 'edit') {
              displayShapes(ctx);
            }            
            planView(ctx);    
          }else{
            displayShapes(ctx);
          }
          detailPop.style.display = 'none';
        }
      }else{
        if (!drawing) {
          ctx.clearRect(0,0,1600,1000);
          if (section == 'view' || section == 'edit') {
            if (section == 'edit') {
              displayShapes(ctx);
            }            
            planView(ctx);    
          }else{
            displayShapes(ctx);
          }          
          detailPop.style.display = 'none';        
        }
      }
    }
  }
}

function pointInCircle(eventType, ctx, x, y){
  planAmenities.forEach(function(amenity){
    points = JSON.parse(amenity.points);
    var dist_points = (x - points['0']) * (x - points['0']) + (y - points['1']) * (y - points['1']);
    if (dist_points < 10) {
      selAmenity = amenity;
    }

  })
  if (selAmenity.points) {
    var points = JSON.parse(selAmenity.points);
    ctx.beginPath();
    ctx.arc(points['0'], points['1'], 10, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.strokeStyle = "#FFF";
    ctx.fillStyle = 'rgba(255,0,0, 0.5)';
    ctx.fill(); 
    var amenities = selAmenity.details.split("\n");
    var displayAmenities = "<ul>";
    amenities.forEach(function(amenity){
      displayAmenities += "<li>" + amenity + "</li>";
    })
    displayAmenities += "</ul>";
    $("#show-amenity-details").html(displayAmenities);
    cancelpopUp();
    var amenityPop = document.getElementById('amenity-pop');
    amenityPop.style.display = 'block';
    amenityPop.style.margin = (points['1']+canvasoffsetTop-10) + 'px ' + '0 0 ' + (points['0']+canvasoffsetLeft+10) + 'px';

  }
}

function inDrawing(x, y, rect){
  if (x >= rect['0'] && x <= rect['2'] && y >= rect['1'] && y <= rect['3']) {
    return true;
  }else{
    return false;
  }
}

function cancelpopUp(){
  var detailPop = document.getElementById('detail-pop');
  detailPop.style.display = 'none';
  popUpSelect = false;
}

function cancelAmenity(){
  var amenityPop = document.getElementById('amenity-pop');
  amenityPop.style.display = 'none';
  selAmenity = {};  
}

function editDetails(){
  $('#editDetails').modal('show');
}
function colorSetting(){
  $('#colorSetting').modal('show');
}

Filevalidation = () => { 
  var reader = new FileReader();

  //Read the contents of Image File.
  var fileUpload = document.getElementById('file');
  reader.readAsDataURL(fileUpload.files[0]);
  reader.onload = function (e) {

    //Initiate the JavaScript Image object.
    var image = new Image();

    //Set the Base64 string return from FileReader as source.
    image.src = e.target.result;
    base64Img = e.target.result;
    document.querySelector("#drawMakersCanvas").style.background = "url('" + base64Img +"') left top/" + 1000*canvasScale + "px auto no-repeat";

    //Validate the File Height and Width.
    image.onload = function () {
      var height = this.height;
      var width = this.width;
      // alert(height + '-' + width);
      return true;
    };
  }
} 

function dataPost(){
  $('.saving').addClass('fa fa-spinner fa-spin');
  var typologies = ['1bhk','2bhk','2bhks','3bhk','studio'];
  var postData = [];
  typologies.forEach(function(typology){
    var data = JSON.parse(localStorage.getItem(typology));
    if (data) {
      switch(typology) {
        case "1bhk":
          postData.push({"bhk1":{data}});
          break;
        case "2bhk":
          postData.push({"bhk2":{data}});
          break;
        case "2bhks":
          postData.push({"bhks2":{data}});
          break;
        case "3bhk":
          postData.push({"bhk3":{data}});
          break;
         case "studio":
          postData.push({"studio":{data}});
          break;
      }
    }    
  })
  var image = document.getElementById('file');
  $.ajax({
      type: "POST",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/new-plan",
      data: {'plan_img':base64Img,'shapes':JSON.stringify(postData)},
      complete: function (data) {
        
      },
      success: function (data) {
        $('.saving').removeClass('fa fa-spinner fa-spin');
        if (data.status == 'success') {
          window.location.replace('#edit/' + data.plan_id);
        }        
      },
  });  
}

function getPlan(planId){
  var status = false;
  $.ajax({
      type: "GET",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/plan/" + planId,
      complete: function (data) {
        
      },
      success: function (data) {
        planAmenities = data.amenities;
        var bhk1 = "";
        var bhk2 = "";
        var bhks2 = "";
        var bhk3 = "";
        var studio = "";    
        data.typologies.forEach(function(typology){
            if (typology.abrv == "1bhk") {
              bhk1 = typology.color;
            }

            if (typology.abrv == "2bhk") {
              bhk2 = typology.color;
            }

            if (typology.abrv == "2bhks") {
              bhks2 = typology.color;
            }

            if (typology.abrv == "3bhk") {
              bhk3 = typology.color;
            }

            if (typology.abrv == "studio") {
              studio = typology.color;
            }          
        })

        markersColors = {'bhk1' : bhk1, 'bhk2' : bhk2, 'bhks2' : bhks2, 'bhk3' : bhk3, 'studio' : studio};
        apiData = data;
        createCanvas(false, false);
      },
  });
}

function saveColorChanges(){
  var color1bhk = document.getElementById('color-1bhk').value;
  var color2bhk = document.getElementById('color-2bhk').value;
  var color2bhks = document.getElementById('color-2bhks').value;
  var color3bhk = document.getElementById('color-3bhk').value;
  var colorStudio = document.getElementById('color-studio').value;  
  markersColors = {'bhk1' : getColorCode(color1bhk), 'bhk2' : getColorCode(color2bhk), 'bhks2' : getColorCode(color2bhks), 'bhk3' : getColorCode(color3bhk), 'studio' : getColorCode(colorStudio)};
  $.ajax({
      type: "POST",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/save-colors",
      data:  markersColors,
      complete: function (data) {
        
      },
      success: function (data) {
        // $('#saving-color').removeClass('fa fa-spinner fa-spin');
      },
  });
  $('#colorSetting').modal('hide');
  createCanvas(false, false);
}

function getColors(){
  $.ajax({
      type: "GET",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/get-colors",
      complete: function (data) {
        
      },
      success: function (data) {
        var bhk1 = "";
        var bhk2 = "";
        var bhks2 = "";
        var bhk3 = "";
        var studio = "";    
        data.forEach(function(item){
            if (item.abrv == "1bhk") {
              bhk1 = item.color;
            }

            if (item.abrv == "2bhk") {
              bhk2 = item.color;
            }

            if (item.abrv == "2bhks") {
              bhks2 = item.color;
            }

            if (item.abrv == "3bhk") {
              bhk3 = item.color;
            }

            if (item.abrv == "studio") {
              studio = item.color;
            }          
        })

        markersColors = {'bhk1' : bhk1, 'bhk2' : bhk2, 'bhks2' : bhks2, 'bhk3' : bhk3, 'studio' : studio};        
      },
  });
}

function addShapes(){
  $('.updating').addClass('fa fa-spinner fa-spin');
  var typologies = ['1bhk','2bhk','2bhks','3bhk','studio'];
  var postData = [];
  typologies.forEach(function(typology){
    var data = JSON.parse(localStorage.getItem(typology));
    if (data) {
      switch(typology) {
        case "1bhk":
          postData.push({"bhk1":{data}});
          break;
        case "2bhk":
          postData.push({"bhk2":{data}});
          break;
        case "2bhks":
          postData.push({"bhks2":{data}});
          break;
        case "3bhk":
          postData.push({"bhk3":{data}});
          break;
         case "studio":
          postData.push({"studio":{data}});
          break;
      }
    }    
  })
  $.ajax({
      type: "POST",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/add-shapes",
      data: {'plan_id': apiData.plan['0'].id,'shapes':JSON.stringify(postData)},
      complete: function (data) {
        
      },
      success: function (data) {
        if (data.status == 'success') {
          window.location.reload();
        }
        $('.saving').removeClass('fa fa-spinner fa-spin');
      },
  });  
}

function saveShapeDetails(){
  $('.saving').removeClass('fa fa-spinner fa-spin');
  var unit = document.getElementById('unit').value;
  var wing = document.getElementById('wing').value;
  var sqmt = document.getElementById('sqmt').value;
  var sqft = document.getElementById('sqft').value;
  var ticketSize  = document.getElementById('ticket-size').value;
  var details = {
      "unit": unit, 
      "wing": wing, 
      "sqmt": sqmt, 
      "sqft": sqft, 
      "ticketSize": ticketSize, 
      "selectedShapeId": selectedShapeId
  };
  $.ajax({
      type: "POST",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/save-shape-details",
      data: details,
      complete: function (data) {
        
      },
      success: function (data) {
        $('.saving').removeClass('fa fa-spinner fa-spin');
        if (data.status == 'success') {
          window.location.reload();
        }
      },
  });  
}

function getColorCode(rgb){
  var rgbCharacters = rgb.split('');
  rgbCharacters.pop();

  var rgbFigures = '';
  for (var i = 0; i <= rgbCharacters.length -1; i++) {
    if (i > 3) {
      rgbFigures += rgbCharacters[i];
    }
  }
  return rgbFigures;
}

function addAmenityDetails(){
  amenityPoints[amenityPoints.length-1]['1'].details = $('#amenity-details').val();
  // console.log(amenityPoints[amenityPoints.length-1]['1'].details);
  $('#amenityFormModal').modal('hide');
}

function addAmenityDismiss(){
  amenityPoints.pop();
  $('#amenityFormModal').modal('hide');
  createCanvas(false, false);
}

function saveAmenityDetails(){
  // $('.updating').addClass('fa fa-spinner fa-spin');
  var typologies = ['1bhk','2bhk','2bhks','3bhk','studio'];
  var postData = [];

  $.ajax({
      type: "POST",
      dataType: 'json',
      url: "http://127.0.0.1:3000/api/add-amenities",
      data: {'plan_id': apiData.plan['0'].id,'amenities':JSON.stringify(amenityPoints)},
      complete: function (data) {
        
      },
      success: function (data) {
        if (data.status == 'success') {
          window.location.reload();
        }
        // $('.saving').removeClass('fa fa-spinner fa-spin');
      },
  });  
}